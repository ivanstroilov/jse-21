package ru.t1.stroilov.tm.api.service;

public interface ISaltProvider {

    Integer getPasswordIteration();

    String getPasswordSecret();

}
