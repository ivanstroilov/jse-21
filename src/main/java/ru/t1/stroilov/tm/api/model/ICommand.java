package ru.t1.stroilov.tm.api.model;

import ru.t1.stroilov.tm.enumerated.Role;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute();

    Role[] getRoles();

}
