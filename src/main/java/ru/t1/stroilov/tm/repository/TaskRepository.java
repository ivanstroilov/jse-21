package ru.t1.stroilov.tm.repository;

import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.model.Task;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    protected Predicate<Task> filterByProjectId(final String projectId) {
        return m -> projectId.equals(m.getId());
    }

    @Override
    public Task add(final String userId, final String name, final String description) {
        final Task task = new Task(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task add(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    public List<Task> findAllByProjectID(final String userId, final String projectId) {
        return findAll().stream().filter(filterByUserId(userId)).filter(filterByProjectId(projectId)).collect(Collectors.toList());
    }


}
