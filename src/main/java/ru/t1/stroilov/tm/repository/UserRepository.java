package ru.t1.stroilov.tm.repository;

import ru.t1.stroilov.tm.api.repository.IUserRepository;
import ru.t1.stroilov.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return findAll().stream().filter(m -> login.equals(m.getLogin())).findFirst().orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return findAll().stream().filter(m -> email.equals(m.getEmail())).findFirst().orElse(null);
    }

    @Override
    public User delete(final User user) {
        if (user == null) return null;
        models.remove(user);
        return user;
    }

    @Override
    public Boolean loginExists(final String login) {
        return findAll().stream().anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public Boolean emailExists(final String email) {
        return findAll().stream().anyMatch(m -> email.equals(m.getEmail()));
    }

    @Override
    public User findById(final String id) {
        return findAll().stream().filter(filterById(id)).findFirst().orElse(null);
    }
}
