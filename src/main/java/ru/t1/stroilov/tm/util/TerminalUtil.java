package ru.t1.stroilov.tm.util;

import ru.t1.stroilov.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String string = SCANNER.nextLine();
        try {
            return Integer.parseInt(string);
        } catch (final RuntimeException e) {
            throw new NumberIncorrectException(string, e);
        }
    }

}
