package ru.t1.stroilov.tm.service;

import ru.t1.stroilov.tm.api.repository.IRepository;
import ru.t1.stroilov.tm.api.service.IService;
import ru.t1.stroilov.tm.exception.field.IdEmptyException;
import ru.t1.stroilov.tm.exception.field.IndexIncorrectException;
import ru.t1.stroilov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public void deleteAll(final Collection<M> collection) {
        repository.deleteAll(collection);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public M add(final M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public M findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Override
    public M findByIndex(final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public M delete(M model) {
        if (model == null) return null;
        return repository.delete(model);
    }

    @Override
    public M deleteById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.deleteById(id);
    }

    @Override
    public M deleteByIndex(final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        return repository.deleteByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }
}
