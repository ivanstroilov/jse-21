package ru.t1.stroilov.tm.service;

import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.api.repository.IUserRepository;
import ru.t1.stroilov.tm.api.service.IPropertyService;
import ru.t1.stroilov.tm.api.service.IUserService;
import ru.t1.stroilov.tm.enumerated.Role;
import ru.t1.stroilov.tm.exception.entity.UserNotFoundException;
import ru.t1.stroilov.tm.exception.field.*;
import ru.t1.stroilov.tm.exception.user.EmailExistsException;
import ru.t1.stroilov.tm.exception.user.LoginExistsException;
import ru.t1.stroilov.tm.model.User;
import ru.t1.stroilov.tm.util.HashUtil;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    private final IPropertyService propertyService;

    public UserService(final IUserRepository repository, final ITaskRepository taskRepository, final IProjectRepository projectRepository, final IPropertyService propertyService) {
        super(repository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.propertyService = propertyService;
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (loginExists(login)) throw new LoginExistsException();
        if (login == null || login.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (loginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (emailExists(email)) throw new EmailExistsException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (loginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = repository.findByLogin(login);
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = repository.findByEmail(email);
        return user;
    }

    @Override
    public User deleteByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        return delete(user);
    }

    @Override
    public User deleteByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = findByEmail(email);
        return delete(user);
    }

    @Override
    public User delete(final User model) {
        if (model == null) throw new UserNotFoundException();
        final User user = super.delete(model);
        if (user == null) throw new UserNotFoundException();
        final String userId = user.getId();
        taskRepository.deleteAll(userId);
        projectRepository.deleteAll(userId);
        return user;
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @Override
    public User updateUser(final String id, final String firstName, final String lastName, final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean loginExists(final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.loginExists(login);
    }

    @Override
    public Boolean emailExists(final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.emailExists(email);
    }

    @Override
    public void lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
    }
}
