package ru.t1.stroilov.tm.command.system;

import ru.t1.stroilov.tm.api.service.ICommandService;
import ru.t1.stroilov.tm.command.AbstractCommand;
import ru.t1.stroilov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
